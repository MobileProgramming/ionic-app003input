import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  flag: boolean = false;
  input: string = '';

  constructor(public navCtrl: NavController) {

  }

  some(x: string) {
    this.input = x;
  }

  some2() {
      return this.input;
  }
}
